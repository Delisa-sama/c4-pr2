#ifndef BD_H
#define BD_H

#include "cortages.h"
#include "file.h"
#include <fstream>
#include <vector>

std::string dbPath( "dbFile.txt" );

std::fstream		 file;
std::vector<cartage> crtgs;

void close_db();

void rewrite_db();

void write_db();

void open_db();

void read_db();

#endif // BD_H
