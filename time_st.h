#ifndef TIME_ST_H
#define TIME_ST_H
#include <iostream>
#include <fstream>

struct time_st {
	time_st() {}

	explicit time_st( const char *s ) {
		uint buf_hours;
		uint buf_mins;
		char buf_sep;

		if ( sscanf( s, "%u %c %u", &buf_hours, &buf_sep, &buf_mins ) == -1 ||
			 buf_hours > 23 || buf_mins > 59 ) {
			throw std::invalid_argument( "" );
		} else {
			m_hours = buf_hours;
			m_mins  = buf_mins;
			m_sep   = buf_sep;
		}
	}

	uint m_hours;
	uint m_mins;
	char m_sep;

	friend std::ostream &operator<<( std::ostream &stream, const time_st &t ) {
		stream << t.m_hours << t.m_sep << t.m_mins;
		return stream;
	}

	friend std::fstream &operator<<( std::fstream &stream, const time_st &t ) {
		stream << t.m_hours << t.m_sep << t.m_mins;
		return stream;
	}
};

#endif // TIME_ST_H
