#ifndef CORTAGES_H
#define CORTAGES_H

#include "file.h"
#include "time_st.h"
#include <string>
#include <fstream>

#define STR_LEN 255
#define COL_SEP " | "

struct cartage {
	cartage() {}
	cartage( const int num, const int park_num, const time_st *btime,
		const time_st *etime, const int len )
		: m_num( num ), m_park_num( park_num ), m_btime( *btime ),
		  m_etime( *etime ), m_len( len ) {}

	int		m_num;
	int		m_park_num;
	time_st m_btime;
	time_st m_etime;
	int		m_len;

	friend std::fstream &operator<<(
		std::fstream &stream, const cartage &crtg ) {
		stream << crtg.m_num << std::endl
			   << crtg.m_park_num << std::endl
			   << crtg.m_btime << std::endl
			   << crtg.m_etime << std::endl
			   << crtg.m_len << std::endl;

		return stream;
	}

	friend std::ostream &operator<<(
		std::ostream &stream, const cartage &crtg ) {
		stream << crtg.m_num << COL_SEP << crtg.m_park_num << COL_SEP
			   << crtg.m_btime << COL_SEP << crtg.m_etime << COL_SEP
			   << crtg.m_len << std::endl;

		return stream;
	}

	friend std::fstream &operator>>( std::fstream &stream, cartage &crtg ) {
		char buf[STR_LEN] = {'\0'};

		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_num = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_num == 0 ) {
			throw std::invalid_argument( "" );
		}

		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_park_num = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_park_num == 0 ) {
			throw std::invalid_argument( "" );
		}

		flush( buf );
		stream.getline( buf, STR_LEN );
		time_st buf_btime( buf );
		crtg.m_btime = buf_btime;

		flush( buf );
		stream.getline( buf, STR_LEN );

		time_st buf_etime( buf );
		crtg.m_etime = buf_etime;

		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_len = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_len == 0 ) {
			throw std::invalid_argument( "" );
		}

		return stream;
	}

	friend std::istream &operator>>( std::istream &stream, cartage &crtg ) {
		char buf[STR_LEN] = {'\0'};

		std::cout << "Enter bus number." << std::endl;

		fuckenDemolishEndlineSymbol();
		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_num = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_num == 0 ) {
			throw std::invalid_argument( "" );
		}

		std::cout << "Enter parking number." << std::endl;

		fuckenDemolishEndlineSymbol();
		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_park_num = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_park_num == 0 ) {
			throw std::invalid_argument( "" );
		}

		std::cout << "Enter departure time (hh:mm)." << std::endl;

		fuckenDemolishEndlineSymbol();
		flush( buf );
		stream.getline( buf, STR_LEN );

		time_st buf_btime( buf );
		crtg.m_btime = buf_btime;

		std::cout << "Enter arrive time (hh:mm)." << std::endl;

		fuckenDemolishEndlineSymbol();
		flush( buf );
		stream.getline( buf, STR_LEN );

		time_st buf_etime( buf );
		crtg.m_etime = buf_etime;

		std::cout << "Enter length of the route." << std::endl;

		fuckenDemolishEndlineSymbol();
		flush( buf );
		stream.getline( buf, STR_LEN );
		crtg.m_len = atoi( buf );

		if ( ( buf[0] != '0' || std::string( buf ).size() > 0 ) &&
			 crtg.m_len == 0 ) {
			throw std::invalid_argument( "" );
		}

		return stream;
	}
};

#endif // CORTAGES_H
